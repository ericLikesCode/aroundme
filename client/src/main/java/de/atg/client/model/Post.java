import java.util.HashMap;
/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 16.12.2019
 * @author 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {
  
  private PostOrigin origin;
  private Map<PostOrigin, HashMap<String, String>> postValues = new HashMap<>();
  
  public PostOrigin getOrigin() {
    return origin;
  }
  
  public void setOrigin(PostOrigin newOrigin) {
    this.origin = newOrigin;
  }
  
  public Map<PostOrigin, HashMap<String, String>> getEveryPostValue() {
    return postValues;
  }
  
  public HashMap<String, String> getValues(PostOrigin origin) {
    return postValues.get(origin);
  }
  
  public void setPostValues(HashMap<String, String> values) {
    postValues = values;
  }
  
  public void editPostValues(PostOrigin origin, HashMap<String, String> newValues) {
    postValues.remove(origin);
    postValues.put(origin, newValues);
  }
  
  public void editPostValue(PostOrigin origin, String key, String value) {
    HashMap<String, String> oldVal = postValues.get(origin);
    oldVal.put(key, value);
    postValues.put(origin, oldVal);
  }







  
} 

